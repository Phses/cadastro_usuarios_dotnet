namespace Testes {
    [TestClass]
   public class TestVerificadorDeSenhas {
        
        [TestMethod]
        public void TestSenhaContemUmNumero() {
            Cadastro cadastro = Cadastro.cadastro;
            Usuario pedro = new Usuario("pedro@gmail.com", "daodkokoM*", "daodkokoM*");
            Usuario joao = new Usuario("joao@gmail.com", "123asdfkM*", "123asdfkM*");
            CollectionAssert.AreEqual(new List<Status> {Status.SENHA_NAO_CONTEM_NUMEROS}, cadastro.CadastrarUsuario(pedro));
            CollectionAssert.AreEqual(new List<Status> {Status.SENHA_VALIDA, Status.USUARIO_CADASTRADO}, cadastro.CadastrarUsuario(joao));
        }
        [TestMethod]
        public void TesteSenhaContem8caracteres() {
            Cadastro cadastro = Cadastro.cadastro;
            Usuario pedro = new Usuario("pedro@gmail.com", "2345M", "2345M");
            Usuario joao = new Usuario("joao@gmail.com", "12345678M*", "12345678M*");

            CollectionAssert.AreEqual(new List<Status>{Status.SENHA_NAO_CONTEM_8CARACTERES, Status.SENHA_NAO_CONTEM_CARACTER_ESPECIAL},  cadastro.CadastrarUsuario(pedro));
            CollectionAssert.AreEqual(new List<Status> {Status.SENHA_VALIDA, Status.USUARIO_CADASTRADO}, cadastro.CadastrarUsuario(joao));
        }
        [TestMethod]
        public void TesteSenhaContemUmaLetraMaiuscula() {
            Cadastro cadastro = Cadastro.cadastro;
            Usuario pedro = new Usuario("pedro@gmail.com", "2345", "2345");
            Usuario joao = new Usuario("joao@gmail.com", "12345678M*", "12345678M*");
            CollectionAssert.AreEqual(new List<Status>{Status.SENHA_NAO_CONTEM_8CARACTERES, Status.SENHA_NAO_CONTEM_LETRA_MAIUSCULA, Status.SENHA_NAO_CONTEM_CARACTER_ESPECIAL},  cadastro.CadastrarUsuario(pedro));
            CollectionAssert.AreEqual(new List<Status> {Status.SENHA_VALIDA, Status.USUARIO_CADASTRADO}, cadastro.CadastrarUsuario(joao));
        }
        [TestMethod]
        public void TesteSenhaContemAoMenosUmCaractereEspecial() {
            Cadastro cadastro = Cadastro.cadastro;
            Usuario pedro = new Usuario("pedro@gmail.com", "2345", "2345");
            Usuario joao = new Usuario("joao@gmail.com", "12345678M*", "12345678M*");
            CollectionAssert.AreEqual(new List<Status>{Status.SENHA_NAO_CONTEM_8CARACTERES, Status.SENHA_NAO_CONTEM_LETRA_MAIUSCULA, Status.SENHA_NAO_CONTEM_CARACTER_ESPECIAL},  cadastro.CadastrarUsuario(pedro));
            CollectionAssert.AreEqual(new List<Status> {Status.SENHA_VALIDA, Status.USUARIO_CADASTRADO}, cadastro.CadastrarUsuario(joao));
        }

    }
}