namespace Testes {

    [TestClass]
    public class CadastroTestes {

        [TestMethod]
        public void TesteUsuarioCadastrado() {
            Cadastro cadastro = Cadastro.cadastro;
            Usuario usuario = new Usuario("pedro@gmail.com", "12345678M*", "12345678M*");
            CollectionAssert.AreEqual(new List<Status> {Status.SENHA_VALIDA, Status.USUARIO_CADASTRADO}, cadastro.CadastrarUsuario(usuario));
        }
        [TestMethod]
        public void TesteSeJaExisteEmailCadastrado() {
            Cadastro cadastro = Cadastro.cadastro;
            Usuario usuario1 = new Usuario("pedro@gmail.com", "12345678M*", "12345678M*");
            Usuario usuario2 = new Usuario("pedro@gmail.com", "123abc5678M*", "123abc5678M*");
            CollectionAssert.AreEqual(new List<Status> {Status.SENHA_VALIDA, Status.USUARIO_CADASTRADO}, cadastro.CadastrarUsuario(usuario1));
            CollectionAssert.AreEqual(new List<Status> {Status.SENHA_VALIDA, Status.EMAIL_EXISTENTE}, cadastro.CadastrarUsuario(usuario2));

        }
        [TestMethod]
        public void TesteSenhasCorrespondem() {
            Cadastro cadastro = Cadastro.cadastro;
            Usuario usuario1 = new Usuario("pedro@gmail.com", "12345678M*", "12345678J*");
            CollectionAssert.AreEqual(new List<Status> {Status.SENHA_VALIDA, Status.SENHAS_NAO_CORRESPONDEM}, cadastro.CadastrarUsuario(usuario1));
        }

    }
}