using System.Text.RegularExpressions;

namespace CadastroDeUsuario {
    public class VerificadorDeSenha {

        public static List<Status> VerificaSenha(Usuario usuario) {
            List<Status> ValidacoesSenhas = new List<Status>();
            if(!ContemAoMenosUmNumero(usuario.Senha)) {
                ValidacoesSenhas.Add(Status.SENHA_NAO_CONTEM_NUMEROS);
            }
            if(!Contem8Caracteres(usuario.Senha)) {
                ValidacoesSenhas.Add(Status.SENHA_NAO_CONTEM_8CARACTERES);
            }
            if(!ContemLetraMaiuscula(usuario.Senha)) {
                ValidacoesSenhas.Add(Status.SENHA_NAO_CONTEM_LETRA_MAIUSCULA);
            }
            if(!ContemCaracterEspecial(usuario.Senha)) {
                ValidacoesSenhas.Add(Status.SENHA_NAO_CONTEM_CARACTER_ESPECIAL);
            }
            if(!ValidacoesSenhas.Any()) {
                ValidacoesSenhas.Add(Status.SENHA_VALIDA);
            }
             return ValidacoesSenhas.ToList();
        }
        public static bool ContemAoMenosUmNumero(string senha) => Regex.Match(senha, @"[0-9]").Success;
        public static bool Contem8Caracteres(string senha) => (senha.Length > 7);
        public static bool ContemLetraMaiuscula(string senha) => Regex.Match(senha, @"[A-Z]").Success;
        public static bool ContemCaracterEspecial(string senha) => Regex.Match(senha, @"[!@#\$%\^&\*\(\)]").Success; 
    }
}
