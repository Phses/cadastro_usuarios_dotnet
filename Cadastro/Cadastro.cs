namespace CadastroDeUsuario {
    public class Cadastro {
        public static Cadastro _cadastro;
        public List<Usuario> Usuarios = new List<Usuario>();
        public List<Status> Validacoes = new List<Status>();

        private Cadastro() {

        }

        public static Cadastro cadastro {
            get {
                if(_cadastro != null) {
                    return _cadastro;
                }
                return new Cadastro();
            }
        }

        public  List<Status> CadastrarUsuario(Usuario usuario) {
            
            Validacoes = VerificadorDeSenha.VerificaSenha(usuario);
            if(Validacoes.Contains(Status.SENHA_VALIDA)) {
                if(EmailInvalido(usuario)) {
                    Validacoes.Add(Status.EMAIL_EXISTENTE);
                    return Validacoes;
                }
                if(!SenhasCorrespondem(usuario)) {
                    Validacoes.Add(Status.SENHAS_NAO_CORRESPONDEM);
                    return Validacoes;
                }
                Usuarios.Add(usuario);
                Validacoes.Add(Status.USUARIO_CADASTRADO);
                return Validacoes;
            }
            return Validacoes;
        }

        public bool EmailInvalido(Usuario usuario) => (Usuarios.Where(user => user.Email.Contains(usuario.Email)).Any());
        public bool SenhasCorrespondem(Usuario usuario) => (usuario.Senha.Equals(usuario.ConfirmacaoSenha));
    }
}