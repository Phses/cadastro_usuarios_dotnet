namespace CadastroDeUsuario {
    public class Usuario {
        public string Email { get; set; }
        public string Senha { get; set; }
        public string ConfirmacaoSenha { get; set; }

        public Usuario(string email, string senha, string confirmacaoSenha) {
            this.Email = email;
            this.Senha = senha;
            this.ConfirmacaoSenha = confirmacaoSenha;
        }
    }
}